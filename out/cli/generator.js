"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateJavaScript = void 0;
const path_1 = __importDefault(require("path"));
const main_generator_1 = require("./LibGenerator/main-generator");
function generateJavaScript(application, filePath, destination) {
    const final_destination = extractDestination(filePath, destination);
    (0, main_generator_1.generateLib)(application, final_destination);
    return final_destination;
}
exports.generateJavaScript = generateJavaScript;
function extractDestination(filePath, destination) {
    const path_ext = new RegExp(path_1.default.extname(filePath) + '$', 'g');
    filePath = filePath.replace(path_ext, '');
    return destination !== null && destination !== void 0 ? destination : path_1.default.join(path_1.default.dirname(filePath), "generated");
}
//# sourceMappingURL=generator.js.map