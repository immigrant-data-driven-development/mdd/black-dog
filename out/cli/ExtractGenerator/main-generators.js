"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateExtract = void 0;
const helpers_generators_1 = require("./module/helpers-generators");
const source_generators_1 = require("./module/source-generators");
function generateExtract(application, target_folder) {
    //generateDocumentation(application,target_folder)
    (0, source_generators_1.generateSource)(application, target_folder);
    (0, helpers_generators_1.generateHelpers)(application, target_folder);
}
exports.generateExtract = generateExtract;
//# sourceMappingURL=main-generators.js.map