"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateHelpers = void 0;
const fs_1 = __importDefault(require("fs"));
const langium_1 = require("langium");
const generator_utils_1 = require("../../generator-utils");
const path_1 = __importDefault(require("path"));
function generateHelpers(application, target_folder) {
    fs_1.default.mkdirSync(target_folder, { recursive: true });
    const EXTRACT_PATH = (0, generator_utils_1.createPath)(target_folder, "extract");
    const SRC_PATH = (0, generator_utils_1.createPath)(EXTRACT_PATH, "src");
    if (application.configuration) {
        fs_1.default.writeFileSync(path_1.default.join(SRC_PATH, '.env'), createEnv(application.configuration));
        fs_1.default.writeFileSync(path_1.default.join(SRC_PATH, 'docker-compose.yml'), createDockerCompose(application.configuration));
    }
    fs_1.default.writeFileSync(path_1.default.join(SRC_PATH, '__init__.py'), "");
    fs_1.default.writeFileSync(path_1.default.join(SRC_PATH, 'Dockerfile'), createDockerFile());
    fs_1.default.writeFileSync(path_1.default.join(SRC_PATH, 'run.sh'), createRun());
}
exports.generateHelpers = generateHelpers;
function createRun() {
    return "python application.py";
}
function createDockerFile() {
    return (0, langium_1.expandToString) `
    FROM python:3.10-bullseye

    WORKDIR /app

    COPY requirements.txt requirements.txt
    RUN pip install --no-cache-dir -r requirements.txt

    COPY . .

    CMD [ "python3", "application.py"]
    `;
}
function createDockerCompose(configuration) {
    return (0, langium_1.expandToString) `
    version: '3.9'
    services:
      extract:
        container_name: ${configuration.software_name}_extract
        build: .
        networks: 
          - broker-kafka
    
    networks: 
      broker-kafka:
        name: infra
        external: true        
        driver: bridge  
    `;
}
function createEnv(configuration) {
    return (0, langium_1.expandToString) `
    TOPIC =  "${configuration.topic}"
    SERVERS =   "${configuration.kafka_servers}"
    GROUP_ID =  "${configuration.group_id}"

    `;
}
//# sourceMappingURL=helpers-generators.js.map