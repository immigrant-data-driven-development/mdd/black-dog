"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateDocumentation = void 0;
const fs_1 = __importDefault(require("fs"));
const langium_1 = require("langium");
const generator_utils_1 = require("../../generator-utils");
const ast_1 = require("../../../language-server/generated/ast");
const path_1 = __importDefault(require("path"));
const ident = generator_utils_1.base_ident;
function generateDocumentation(model, target_folder) {
    var _a;
    fs_1.default.mkdirSync(target_folder, { recursive: true });
    const LIB_PATH = (0, generator_utils_1.createPath)(target_folder, "lib");
    if (model.configuration) {
        fs_1.default.writeFileSync(path_1.default.join(LIB_PATH, 'README.md'), createProjectReadme(model.configuration));
    }
    const DOCS_PATH = (0, generator_utils_1.createPath)(LIB_PATH, "docs");
    const modules = model.abstractElements.filter(ast_1.isModule);
    fs_1.default.writeFileSync(path_1.default.join(DOCS_PATH, "README.md"), generalREADME(modules));
    fs_1.default.writeFileSync(path_1.default.join(DOCS_PATH, "packagediagram.puml"), createPackageDiagram(modules, (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.software_name));
    for (const m of modules) {
        const MODULE_PATH = (0, generator_utils_1.createPath)(DOCS_PATH, m.name.toLowerCase());
        fs_1.default.writeFileSync(path_1.default.join(MODULE_PATH, "/README.md"), moduleREADME(m));
        fs_1.default.writeFileSync(path_1.default.join(MODULE_PATH, "/classdiagram.puml"), createClassDiagram(m));
    }
}
exports.generateDocumentation = generateDocumentation;
function createPackageDiagram(modules, name) {
    const lines = [
        `@startuml ${name !== null && name !== void 0 ? name : ''}`,
        ...modules.flatMap(m => [
            `namespace ${m.name} {`,
            ``,
            `}`,
        ]),
        `@enduml`,
        ``
    ];
    return lines.join('\n');
}
function createClassDiagram(m) {
    const enums = m.elements.filter(ast_1.isEnumX);
    const entities = m.elements.filter(ast_1.isLocalEntity);
    const lines = [
        `@startuml ${m.name}`,
        ...enums.flatMap(e => [
            `enum ${e.name} {`,
            ...e.attributes.map(a => `${ident}${a.name}`),
            `}`,
        ]),
        ``,
        ...entities.map(e => entityClassDiagram(e, m)),
        `@enduml`,
        ``
    ];
    return lines.join('\n');
}
function entityClassDiagram(e, m) {
    var _a;
    const lines = [
        `class ${e.name} {`,
        ...e.attributes.map(a => `${a.type}: ${a.name}`),
        ``,
        ...e.relations.filter(r => !(0, ast_1.isManyToMany)(r)).map(r => { var _a; return `${(_a = r.type.ref) === null || _a === void 0 ? void 0 : _a.name}: ${r.name.toLowerCase()}`; }),
        `}`,
        ((_a = e.superType) === null || _a === void 0 ? void 0 : _a.ref) ? `\n${e.superType.ref.name} <|-- ${e.name}\n` : '',
        e.enumentityatributes.map(a => { var _a; return `${e.name} "1" -- "1" ${(_a = a.type.ref) === null || _a === void 0 ? void 0 : _a.name} : ${a.name.toLowerCase()}>`; }),
        ...e.relations.filter(r => !(0, ast_1.isManyToOne)(r)).map(r => relationDiagram(r, e, m)),
        ``
    ];
    return lines.join('\n');
}
function relationDiagram(r, e, m) {
    var _a, _b, _c;
    // Cardinalidades
    const tgt_card = (0, ast_1.isOneToOne)(r) ? "1" : "0..*";
    const src_card = (0, ast_1.isManyToMany)(r) ? "0..*" : "1";
    // Módulo de origem da entidade destino
    const origin_module = ((_a = r.type.ref) === null || _a === void 0 ? void 0 : _a.$container.name.toLowerCase()) !== m.name.toLowerCase() ?
        `${(_b = r.type.ref) === null || _b === void 0 ? void 0 : _b.$container.name}.` :
        "";
    return `${e.name} "${src_card}" -- "${tgt_card}" ${origin_module}${(_c = r.type.ref) === null || _c === void 0 ? void 0 : _c.name} : ${r.name.toLowerCase()} >`;
}
function moduleREADME(m) {
    var _a;
    const lines = [
        `# 📕Documentation: ${m.name}`,
        ``,
        `${(_a = m.description) !== null && _a !== void 0 ? _a : ''}`,
        ``,
        `## 🌀 Package's Data Model`,
        ``,
        `![Domain Diagram](classdiagram.png)`,
        ``,
        `### ⚡Entities`,
        ``,
        ...m.elements.filter(ast_1.isLocalEntity).map(e => { var _a; return `* **${e.name}** : ${(_a = e.description) !== null && _a !== void 0 ? _a : '-'}`; }),
        ``
    ];
    return lines.join('\n');
}
function generalREADME(modules) {
    return (0, langium_1.expandToStringWithNL) `
    # 📕Documentation

    ## 🌀 Project's Package Model
    ![Domain Diagram](packagediagram.png)

    ### 📲 Modules
    ${modules.map(m => { var _a; return `* **[${m.name}](./${m.name.toLocaleLowerCase()}/)** :${(_a = m.description) !== null && _a !== void 0 ? _a : '-'}`; }).join("\n")}

    `;
}
function createProjectReadme(configuration) {
    return (0, langium_1.expandToStringWithNL) `
    # ${configuration.software_name}
    ## 🚀 Goal
    ${configuration.about}

    ## 📕Documentation
    
    The Documentation can be found [here](./docs/README.md)

    ## ⚙️ Requirements

    ## 🔧 Install
    
    ## ✒️ Team
    
    * **[${configuration.author}](${configuration.author_email})**
    
    ## 📕 Literature

    `;
}
//# sourceMappingURL=documentation-generator.js.map