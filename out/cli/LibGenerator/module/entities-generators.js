"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateEntities = void 0;
const fs_1 = __importDefault(require("fs"));
const generator_utils_1 = require("../../generator-utils");
const ast_1 = require("../../../language-server/generated/ast");
const path_1 = __importDefault(require("path"));
const langium_1 = require("langium");
function generateEntities(application, target_folder) {
    fs_1.default.mkdirSync(target_folder, { recursive: true });
    if (application.configuration) {
        const application_name = application.configuration.software_name;
        const LIB_PATH = (0, generator_utils_1.createPath)(target_folder, "lib");
        const SOFTWARE_NAME_PATH = (0, generator_utils_1.createPath)(LIB_PATH, application_name);
        fs_1.default.writeFileSync(path_1.default.join(SOFTWARE_NAME_PATH, "__init__.py"), "");
        let entities = [];
        for (const module of application.abstractElements.filter(ast_1.isModule)) {
            for (const entity of module.elements.filter(ast_1.isLocalEntity)) {
                entities.push(entity);
                fs_1.default.writeFileSync(path_1.default.join(SOFTWARE_NAME_PATH, `${entity.name.toLowerCase()}.py`), generateModel(entity));
            }
        }
        fs_1.default.writeFileSync(path_1.default.join(SOFTWARE_NAME_PATH, `factories.py`), generateFactory(entities));
    }
    function generateFactoryClass(entity) {
        return (0, langium_1.expandToString) `
        class ${entity.name}Factory(factory.Factory):
            class Meta:
                model = ${entity.name}
        `;
    }
    function generateFactory(entities) {
        return (0, langium_1.expandToString) `
        import factory
        ${entities.map(entity => `import .${entity.name.toLowerCase()} import ${entity.name}`).join("\n")}

        ${entities.map(entity => generateFactoryClass(entity)).join("\n")}
        
        `;
    }
    function generateModel(entity) {
        return (0, langium_1.expandToString) `
        import logging
        logging.basicConfig(level=logging.INFO)
        class ${entity.name}():
            def __init__(self):
                pass
            
            def get_all(self,today = False):
                try:
                    results = []
			        logging.info("Start function: get_all: ${entity.name}")
                    return results
                except Exception as e: 
                    logging.error("OS error: {0}".format(e))
                    logging.error(e.__dict__)
        `;
    }
}
exports.generateEntities = generateEntities;
//# sourceMappingURL=entities-generators.js.map