"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateLib = void 0;
const entities_generators_1 = require("./module/entities-generators");
const helpers_generators_1 = require("./module/helpers-generators");
const documentation_generator_1 = require("./module/documentation-generator");
function generateLib(application, target_folder) {
    (0, documentation_generator_1.generateDocumentation)(application, target_folder);
    (0, entities_generators_1.generateEntities)(application, target_folder);
    (0, helpers_generators_1.generateHelpers)(application, target_folder);
}
exports.generateLib = generateLib;
//# sourceMappingURL=main-generator.js.map