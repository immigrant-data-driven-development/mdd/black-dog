"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlackDogValidator = exports.registerValidationChecks = void 0;
/**
 * Register custom validation checks.
 */
function registerValidationChecks(services) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.BlackDogValidator;
    const checks = {
    //Person: validator.checkPersonStartsWithCapital
    };
    registry.register(checks, validator);
}
exports.registerValidationChecks = registerValidationChecks;
/**
 * Implementation of custom validations.
 */
class BlackDogValidator {
}
exports.BlackDogValidator = BlackDogValidator;
//# sourceMappingURL=black-dog-validator.js.map