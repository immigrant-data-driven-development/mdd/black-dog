import { ValidationChecks } from 'langium';
import { BlackDogAstType } from './generated/ast';
import type { BlackDogServices } from './black-dog-module';

/**
 * Register custom validation checks.
 */
export function registerValidationChecks(services: BlackDogServices) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.BlackDogValidator;
    const checks: ValidationChecks<BlackDogAstType> = {
        //Person: validator.checkPersonStartsWithCapital
    };
    registry.register(checks, validator);
}

/**
 * Implementation of custom validations.
 */
export class BlackDogValidator {


}
