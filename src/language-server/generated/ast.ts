/******************************************************************************
 * This file was generated by langium-cli 1.1.0.
 * DO NOT EDIT MANUALLY!
 ******************************************************************************/

/* eslint-disable */
import { AstNode, AbstractAstReflection, Reference, ReferenceInfo, TypeMetaData } from 'langium';

export type AbstractElement = EnumX | Module;

export const AbstractElement = 'AbstractElement';

export function isAbstractElement(item: unknown): item is AbstractElement {
    return reflection.isInstance(item, AbstractElement);
}

export type DATATYPE = 'boolean' | 'char' | 'date' | 'datetime' | 'decimal' | 'integer' | 'string' | 'uuid';

export type Entity = LocalEntity;

export const Entity = 'Entity';

export function isEntity(item: unknown): item is Entity {
    return reflection.isInstance(item, Entity);
}

export type QualifiedName = string;

export type QualifiedNameWithWildcard = string;

export type Relation = ManyToMany | ManyToOne | OneToMany | OneToOne;

export const Relation = 'Relation';

export function isRelation(item: unknown): item is Relation {
    return reflection.isInstance(item, Relation);
}

export interface Application extends AstNode {
    readonly $type: 'Application';
    abstractElements: Array<AbstractElement>
    configuration?: Configuration
}

export const Application = 'Application';

export function isApplication(item: unknown): item is Application {
    return reflection.isInstance(item, Application);
}

export interface Attribute extends AstNode {
    readonly $container: LocalEntity;
    readonly $type: 'Attribute';
    description?: string
    fullName?: string
    name: string
    notRequired: boolean
    type: DATATYPE
}

export const Attribute = 'Attribute';

export function isAttribute(item: unknown): item is Attribute {
    return reflection.isInstance(item, Attribute);
}

export interface AttributeEnum extends AstNode {
    readonly $container: EnumX;
    readonly $type: 'AttributeEnum';
    description?: string
    fullName?: string
    name: string
}

export const AttributeEnum = 'AttributeEnum';

export function isAttributeEnum(item: unknown): item is AttributeEnum {
    return reflection.isInstance(item, AttributeEnum);
}

export interface Configuration extends AstNode {
    readonly $container: Application;
    readonly $type: 'Configuration';
    about: string
    author: string
    author_email: string
    extract_lib: string
    group_id: string
    kafka_servers: string
    repository: string
    software_name: string
    topic: string
}

export const Configuration = 'Configuration';

export function isConfiguration(item: unknown): item is Configuration {
    return reflection.isInstance(item, Configuration);
}

export interface EnumEntityAtribute extends AstNode {
    readonly $container: LocalEntity;
    readonly $type: 'EnumEntityAtribute';
    description?: string
    name: string
    type: Reference<EnumX>
}

export const EnumEntityAtribute = 'EnumEntityAtribute';

export function isEnumEntityAtribute(item: unknown): item is EnumEntityAtribute {
    return reflection.isInstance(item, EnumEntityAtribute);
}

export interface EnumX extends AstNode {
    readonly $container: Application | Module;
    readonly $type: 'EnumX';
    attributes: Array<AttributeEnum>
    description?: string
    name: string
}

export const EnumX = 'EnumX';

export function isEnumX(item: unknown): item is EnumX {
    return reflection.isInstance(item, EnumX);
}

export interface LocalEntity extends AstNode {
    readonly $container: Module;
    readonly $type: 'LocalEntity';
    attributes: Array<Attribute>
    description?: string
    enumentityatributes: Array<EnumEntityAtribute>
    is_abstract: boolean
    name: string
    relations: Array<Relation>
    superType?: Reference<Entity>
}

export const LocalEntity = 'LocalEntity';

export function isLocalEntity(item: unknown): item is LocalEntity {
    return reflection.isInstance(item, LocalEntity);
}

export interface ManyToMany extends AstNode {
    readonly $container: LocalEntity;
    readonly $type: 'ManyToMany';
    by?: Reference<LocalEntity>
    description?: string
    fullName?: string
    name: string
    type: Reference<Entity>
}

export const ManyToMany = 'ManyToMany';

export function isManyToMany(item: unknown): item is ManyToMany {
    return reflection.isInstance(item, ManyToMany);
}

export interface ManyToOne extends AstNode {
    readonly $container: LocalEntity;
    readonly $type: 'ManyToOne';
    description?: string
    fullName?: string
    name: string
    type: Reference<Entity>
}

export const ManyToOne = 'ManyToOne';

export function isManyToOne(item: unknown): item is ManyToOne {
    return reflection.isInstance(item, ManyToOne);
}

export interface Module extends AstNode {
    readonly $container: Application | Module;
    readonly $type: 'Module';
    description?: string
    elements: Array<AbstractElement | LocalEntity>
    name: QualifiedName
}

export const Module = 'Module';

export function isModule(item: unknown): item is Module {
    return reflection.isInstance(item, Module);
}

export interface OneToMany extends AstNode {
    readonly $container: LocalEntity;
    readonly $type: 'OneToMany';
    description?: string
    fullName?: string
    name: string
    type: Reference<Entity>
}

export const OneToMany = 'OneToMany';

export function isOneToMany(item: unknown): item is OneToMany {
    return reflection.isInstance(item, OneToMany);
}

export interface OneToOne extends AstNode {
    readonly $container: LocalEntity;
    readonly $type: 'OneToOne';
    description?: string
    fullName?: string
    name: string
    type: Reference<Entity>
}

export const OneToOne = 'OneToOne';

export function isOneToOne(item: unknown): item is OneToOne {
    return reflection.isInstance(item, OneToOne);
}

export interface BlackDogAstType {
    AbstractElement: AbstractElement
    Application: Application
    Attribute: Attribute
    AttributeEnum: AttributeEnum
    Configuration: Configuration
    Entity: Entity
    EnumEntityAtribute: EnumEntityAtribute
    EnumX: EnumX
    LocalEntity: LocalEntity
    ManyToMany: ManyToMany
    ManyToOne: ManyToOne
    Module: Module
    OneToMany: OneToMany
    OneToOne: OneToOne
    Relation: Relation
}

export class BlackDogAstReflection extends AbstractAstReflection {

    getAllTypes(): string[] {
        return ['AbstractElement', 'Application', 'Attribute', 'AttributeEnum', 'Configuration', 'Entity', 'EnumEntityAtribute', 'EnumX', 'LocalEntity', 'ManyToMany', 'ManyToOne', 'Module', 'OneToMany', 'OneToOne', 'Relation'];
    }

    protected override computeIsSubtype(subtype: string, supertype: string): boolean {
        switch (subtype) {
            case EnumX:
            case Module: {
                return this.isSubtype(AbstractElement, supertype);
            }
            case LocalEntity: {
                return this.isSubtype(Entity, supertype);
            }
            case ManyToMany:
            case ManyToOne:
            case OneToMany:
            case OneToOne: {
                return this.isSubtype(Relation, supertype);
            }
            default: {
                return false;
            }
        }
    }

    getReferenceType(refInfo: ReferenceInfo): string {
        const referenceId = `${refInfo.container.$type}:${refInfo.property}`;
        switch (referenceId) {
            case 'EnumEntityAtribute:type': {
                return EnumX;
            }
            case 'LocalEntity:superType':
            case 'ManyToMany:type':
            case 'ManyToOne:type':
            case 'OneToMany:type':
            case 'OneToOne:type': {
                return Entity;
            }
            case 'ManyToMany:by': {
                return LocalEntity;
            }
            default: {
                throw new Error(`${referenceId} is not a valid reference id.`);
            }
        }
    }

    getTypeMetaData(type: string): TypeMetaData {
        switch (type) {
            case 'Application': {
                return {
                    name: 'Application',
                    mandatory: [
                        { name: 'abstractElements', type: 'array' }
                    ]
                };
            }
            case 'Attribute': {
                return {
                    name: 'Attribute',
                    mandatory: [
                        { name: 'notRequired', type: 'boolean' }
                    ]
                };
            }
            case 'EnumX': {
                return {
                    name: 'EnumX',
                    mandatory: [
                        { name: 'attributes', type: 'array' }
                    ]
                };
            }
            case 'LocalEntity': {
                return {
                    name: 'LocalEntity',
                    mandatory: [
                        { name: 'attributes', type: 'array' },
                        { name: 'enumentityatributes', type: 'array' },
                        { name: 'is_abstract', type: 'boolean' },
                        { name: 'relations', type: 'array' }
                    ]
                };
            }
            case 'Module': {
                return {
                    name: 'Module',
                    mandatory: [
                        { name: 'elements', type: 'array' }
                    ]
                };
            }
            default: {
                return {
                    name: type,
                    mandatory: []
                };
            }
        }
    }
}

export const reflection = new BlackDogAstReflection();
