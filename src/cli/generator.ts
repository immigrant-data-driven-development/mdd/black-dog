import path from 'path';
import { Application } from '../language-server/generated/ast';
import { generateLib } from './LibGenerator/main-generator';


export function generateJavaScript(application: Application, filePath: string, destination: string | undefined): string {
    
    const final_destination = extractDestination(filePath, destination)
    generateLib(application, final_destination)
    
    return final_destination 
    
}

function extractDestination(filePath: string, destination?: string) : string {
    const path_ext = new RegExp(path.extname(filePath)+'$', 'g')
    filePath = filePath.replace(path_ext, '')
  
    return destination ?? path.join(path.dirname(filePath), "generated")
  }