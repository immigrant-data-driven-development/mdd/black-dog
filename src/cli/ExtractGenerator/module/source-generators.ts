import fs from "fs";
import { expandToString } from "langium";
import { createPath } from '../../generator-utils'
import { Application, Configuration,isLocalEntity, isModule, LocalEntity } from "../../../language-server/generated/ast";
import path from 'path'

export function generateSource(application: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    const EXTRACT_PATH = createPath(target_folder, "extract")
    
    const SRC_PATH = createPath(EXTRACT_PATH, "src")
    const SERVICE_PATH = createPath(SRC_PATH, "services")

    let entities: Array<LocalEntity> = []

    for (const module of application.abstractElements.filter(isModule)){
        for (const entity of module.elements.filter(isLocalEntity)){
            entities.push(entity)
        }
    }

    fs.writeFileSync(path.join(SRC_PATH, '__init__.py'),"")
    fs.writeFileSync(path.join(SRC_PATH, 'application.py'),generateApplication())

    if (application.configuration){
        fs.writeFileSync(path.join(SERVICE_PATH, 'extract.py'),generateExtract(application.configuration, entities))
        fs.writeFileSync(path.join(SERVICE_PATH, 'transform.py'),generateTransform(application.configuration, entities))
        fs.writeFileSync(path.join(SERVICE_PATH, 'fn.py'),generateFunction())
    }


}
function generateTransform(configuration:Configuration, entities: Array<LocalEntity>):string{
    return expandToString`
    from apache_beam.transforms import PTransform
    import apache_beam as beam
    from .fn import FnExtract, FnTuple
    from beam_nuggets.io import kafkaio
    from decouple import config

    class Transform(PTransform):
        def __init__(self):
            self.application = "${configuration.software_name ?? 'Software Name not configured'} "
            self.entities = [${entities.map(entity => `${entity.name.toLowerCase()},`)}]
            self.producer_config = {'bootstrap.servers': config('SERVERS')}
            
        def expand(self, pcoll):
            result = None
            for entity in self.entities:
                result = (
                    pcoll
                    | "Retrieve {}".format(entity) >>beam.ParDo(FnExtract(function_name=entity))
                    | "Transform in Tuple {}".format(entity) >>beam.ParDo(FnTuple())
                    | "Sending  {} to Kafka".format(entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application,entity),servers=config('SERVERS'))
                    
                )
            return result
    
    `
}
function generateFunction():string{
    return expandToString`
    import apache_beam as beam
    from .extract import Extract
    import json
    import uuid 
    from .util import Util

    class FnExtract(beam.DoFn):
    
    """ Abstract Function"""
    def __init__(self,function_name) -> None:

        super().__init__()
        self.application_entity = Extract()
        self.util = Util()
        self.function_name = function_name
        
        
    def process(self, element):
        
        data = json.loads(element[1])
            
        self.application_entity.config (
            entity = self.function_name,
            #TODO: Insert parameter
        )
        
        result = self.application_entity.do()      
        return result
                
    class FnTuple(beam.DoFn):

    def __init__(self) -> None:

        super().__init__()
        self.util = Util()
        
    def process(self, element):
        
        index = str(uuid.uuid4())
        element = json.dumps (element)
        
        return [(index,element)]
        
    `
}

function generateExtract(configuration:Configuration, entities: Array<LocalEntity>):string{
    return expandToString`
    import logging
    from ${configuration.extract_lib?? `Extract Lib not configured`} import factories
    logging.basicConfig(level=logging.INFO)
    from .util import Util
    import uuid
    class Extract():
    """Abstract Class with the main function used to extract data application and save in a MongoDB"""

    def __init__(self):
        self.util = Util()
        self.instance = None
        
    def config (self):
        extract = {
            ${entities.map(entity =>`${entity.name.toLowerCase()}:factories.${entity.name}Factory(),`)}    
        }
        self.entity = entity
        self.instance = extract[self.entity]
    
    def do(self):
        """Main function to retrieve and save data in a MongoDB's collection
        
        Args:
            dict data: credentials (secret, url etc) to connect a application
        
        """
        try:
            logging.info("Start Retrieve Information")
            
            data_extracted = self.instance.get_all(today=False)  
                        
            logging.info("End Returning")

            data_transformed = []
            for data in data_extracted:
                
                data_dict = self.util.object_to_dict(data)
                data_dict['entity'] = self.entity
                data_dict['internal_uuid'] = str(uuid.uuid4())
                data_transformed.append (data_dict)

            return data_transformed
            
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)    
    
    `
}

function generateApplication ():string{
    return expandToString`
    import apache_beam as beam
    from beam_nuggets.io import kafkaio
    from decouple import config
    from services.transform import Transform
    import logging

    logging.basicConfig(level=logging.INFO)

    consumer_config = {"topic": config('TOPIC'),
                    "bootstrap_servers": config('SERVERS'),
                    "group_id": config('GROUP_ID'),}

    with beam.Pipeline(runner='DirectRunner') as pipeline:

        credential = (pipeline|  "Reading messages from Kafka"  >> kafkaio.KafkaConsume(
                                            consumer_config=consumer_config))

        credential | "Extract Data and Send To Kafka" >> Transform()

    `
}