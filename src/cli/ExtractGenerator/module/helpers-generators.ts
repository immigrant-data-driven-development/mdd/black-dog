import fs from "fs";
import { expandToString } from "langium";
import { createPath } from '../../generator-utils'
import { Application, Configuration } from "../../../language-server/generated/ast";
import path from 'path'

export function generateHelpers(application: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    const EXTRACT_PATH = createPath(target_folder, "extract")
    
    const SRC_PATH = createPath(EXTRACT_PATH, "src")

    if (application.configuration){
        fs.writeFileSync(path.join(SRC_PATH, '.env'),createEnv(application.configuration))
        fs.writeFileSync(path.join(SRC_PATH, 'docker-compose.yml'),createDockerCompose(application.configuration))
    }
    fs.writeFileSync(path.join(SRC_PATH, '__init__.py'),"")    
    fs.writeFileSync(path.join(SRC_PATH, 'Dockerfile'),createDockerFile())
    fs.writeFileSync(path.join(SRC_PATH, 'run.sh'),createRun())
    fs.writeFileSync(path.join(SRC_PATH, 'requirements.txt'),createRequirements())

}

function createRequirements():string {
    return expandToString`
    apache-beam==2.44.0
    beam-nuggets==0.18.1
    `
}

function createRun():string {
    return "python application.py"
}

function createDockerFile():string {
    return expandToString`
    FROM python:3.10-bullseye

    WORKDIR /app

    COPY requirements.txt requirements.txt
    RUN pip install --no-cache-dir -r requirements.txt

    COPY . .

    CMD [ "python3", "application.py"]
    `
}

function createDockerCompose(configuration: Configuration):string {
    return expandToString`
    version: '3.9'
    services:
      extract:
        container_name: ${configuration.software_name ?? 'Software Name not configured'}_extract
        build: .
        networks: 
          - broker-kafka
    
    networks: 
      broker-kafka:
        name: infra
        external: true        
        driver: bridge  
    `
}
    

function createEnv(configuration : Configuration):string{
    return expandToString`
    TOPIC =  "${configuration.topic ?? "Topic not configured"}"
    SERVERS =   "${configuration.kafka_servers ?? "Kafka Servers not configured"}"
    GROUP_ID =  "${configuration.group_id ?? "Group ID not configured"}"

    `
}