import { Application } from "../../language-server/generated/ast";
import { generateHelpers } from "./module/helpers-generators";
import { generateSource } from "./module/source-generators";

export function generateExtract(application: Application, target_folder: string) : void {
    
    //generateDocumentation(application,target_folder)
    generateSource(application, target_folder)
    generateHelpers(application,target_folder)
     
}