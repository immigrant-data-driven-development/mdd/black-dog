import fs from "fs";
import { expandToStringWithNL } from "langium";
import { createPath, base_ident } from '../../generator-utils'
import { Application,Configuration, isEnumX, Module, isModule, LocalEntity, isLocalEntity, Relation, isOneToOne, isManyToMany, isManyToOne
 } from "../../../language-server/generated/ast";
import path from 'path'

const ident = base_ident

export function generateDocumentation(model: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    
    const LIB_PATH = createPath(target_folder, "lib")

    if (model.configuration){
        fs.writeFileSync(path.join(LIB_PATH, 'README.md'),createProjectReadme(model.configuration))
    }

    const DOCS_PATH = createPath(LIB_PATH, "docs")
    const modules = model.abstractElements.filter(isModule)
    
    fs.writeFileSync(path.join(DOCS_PATH, "README.md"), generalREADME(modules))
    fs.writeFileSync(path.join(DOCS_PATH, "packagediagram.puml"), createPackageDiagram(modules, model.configuration?.software_name))

    for (const m of modules) {
        const MODULE_PATH = createPath(DOCS_PATH, m.name.toLowerCase())
        fs.writeFileSync(path.join(MODULE_PATH, "/README.md"), moduleREADME(m))
        fs.writeFileSync(path.join(MODULE_PATH, "/classdiagram.puml"), createClassDiagram(m))
    }

  
}

function createPackageDiagram(modules: Module[], name?: string) : string {
    const lines = [
        `@startuml ${name ?? ''}`,
        ...modules.flatMap(m => [
            `namespace ${m.name} {`,
            ``,
            `}`,
        ]),
        `@enduml`,
        ``
    ]

    return lines.join('\n')
}


function createClassDiagram(m: Module) : string {
    const enums = m.elements.filter(isEnumX)
    const entities = m.elements.filter(isLocalEntity)

    const lines = [
        `@startuml ${m.name}`,
        ...enums.flatMap(e => [
            `enum ${e.name} {`,
            ...e.attributes.map(a => `${ident}${a.name}`),
            `}`,
        ]),
        ``,
        ...entities.map(e => entityClassDiagram(e, m)),
        `@enduml`,
        ``
    ]

    return lines.join('\n')
}

function entityClassDiagram(e: LocalEntity, m: Module) : string {
    const lines = [
        `class ${e.name} {`,
        ...e.attributes.map(a =>
            `${a.type}: ${a.name}`
        ),
        ``,
        ...e.relations.filter(r => !isManyToMany(r)).map(r =>
            `${r.type.ref?.name}: ${r.name.toLowerCase()}`
        ),
        `}`,
        e.superType?.ref ? `\n${e.superType.ref.name} <|-- ${e.name}\n` : '',
        e.enumentityatributes.map(a =>
            `${e.name} "1" -- "1" ${a.type.ref?.name} : ${a.name.toLowerCase()}>`,
        ),
        ...e.relations.filter(r => !isManyToOne(r)).map(r => relationDiagram(r, e, m)),
        ``
    ]

    return lines.join('\n')
}

function relationDiagram(r: Relation, e: LocalEntity, m: Module) : string {
    // Cardinalidades
    const tgt_card = isOneToOne(r)   ? "1" : "0..*"
    const src_card = isManyToMany(r) ? "0..*" : "1"
    // Módulo de origem da entidade destino
    const origin_module = r.type.ref?.$container.name.toLowerCase() !== m.name.toLowerCase() ?
        `${r.type.ref?.$container.name}.` :
        ""

    return `${e.name} "${src_card}" -- "${tgt_card}" ${origin_module}${r.type.ref?.name} : ${r.name.toLowerCase()} >`
}


function moduleREADME(m: Module) : string {
    const lines = [
        `# 📕Documentation: ${m.name}`,
        ``,
        `${m.description ?? ''}`,
        ``,
        `## 🌀 Package's Data Model`,
        ``,
        `![Domain Diagram](classdiagram.png)`,
        ``,
        `### ⚡Entities`,
        ``,
        ...m.elements.filter(isLocalEntity).map(e =>
            `* **${e.name}** : ${e.description ?? '-'}`
        ),
        ``
    ]

    return lines.join('\n')
}


function generalREADME(modules: Module[]) : string {
    return expandToStringWithNL`
    # 📕Documentation

    ## 🌀 Project's Package Model
    ![Domain Diagram](packagediagram.png)

    ### 📲 Modules
    ${modules.map(m => `* **[${m.name}](./${m.name.toLocaleLowerCase()}/)** :${m.description ?? '-'}`).join("\n")}

    `
}


function createProjectReadme(configuration: Configuration): string{
    return expandToStringWithNL`
    # ${configuration.software_name}
    ## 🚀 Goal
    ${configuration.about}

    ## 📕Documentation
    
    The Documentation can be found [here](./docs/README.md)

    ## ⚙️ Requirements

    ## 🔧 Install
    
    ## ✒️ Team
    
    * **[${configuration.author}](${configuration.author_email})**
    
    ## 📕 Literature

    `
}