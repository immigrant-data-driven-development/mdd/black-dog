import fs from "fs";
import path from 'path'

import { createPath } from '../../generator-utils'
import { Application, Configuration } from "../../../language-server/generated/ast";
import { expandToString } from "langium";


export function generateHelpers(application: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    const LIB_PATH = createPath(target_folder, "lib")

    //const gitignore = fs.readFileSync(path.join(,'gitignore.txt'), 'utf-8')

    fs.writeFileSync(path.join(LIB_PATH, ".gitignore"), createGitIgnore());
    fs.writeFileSync(path.join(LIB_PATH, ".gitlab-ci.yml"), createGitlab());
    fs.writeFileSync(path.join(LIB_PATH, ".publish.sh"), createPublish());
    fs.writeFileSync(path.join(LIB_PATH, "pyproject.toml"), createPoml());
    fs.writeFileSync(path.join(LIB_PATH, "requirements.txt"), createRequirements());
    
    if (application.configuration){

        fs.writeFileSync(path.join(LIB_PATH, "sonar-project.properties"), createSonarProject(application.configuration));
        fs.writeFileSync(path.join(LIB_PATH, "setup.py"), createSetup(application.configuration));        
    }
}

function createRequirements():string {
    return expandToString`
    factory-boy==3.2.1
    `
}

function createGitIgnore():string{
    return expandToString`
    # Byte-compiled / optimized / DLL files
    __pycache__/
    *.py[cod]
    *$py.class

    # C extensions
    *.so

    # Distribution / packaging
    .Python
    build/
    develop-eggs/
    dist/
    downloads/
    eggs/
    .eggs/
    lib/
    lib64/
    parts/
    sdist/
    var/
    wheels/
    share/python-wheels/
    *.egg-info/
    .installed.cfg
    *.egg
    MANIFEST

    # PyInstaller
    #  Usually these files are written by a python script from a template
    #  before PyInstaller builds the exe, so as to inject date/other infos into it.
    *.manifest
    *.spec

    # Installer logs
    pip-log.txt
    pip-delete-this-directory.txt

    # Unit test / coverage reports
    htmlcov/
    .tox/
    .nox/
    .coverage
    .coverage.*
    .cache
    nosetests.xml
    coverage.xml
    *.cover
    .hypothesis/
    .pytest_cache/

    # Translations
    *.mo
    *.pot

    # Django stuff:
    *.log
    local_settings.py
    db.sqlite3

    # Flask stuff:
    instance/
    .webassets-cache

    # Scrapy stuff:
    .scrapy

    # Sphinx documentation
    docs/_build/

    # PyBuilder
    target/

    # Jupyter Notebook
    .ipynb_checkpoints

    # IPython
    profile_default/
    ipython_config.py

    # pyenv
    .python-version

    # celery beat schedule file
    celerybeat-schedule

    # SageMath parsed files
    *.sage.py

    # Environments
    .env
    .venv
    env/
    venv/
    ENV/
    env.bak/
    venv.bak/

    # Spyder project settings
    .spyderproject
    .spyproject

    # Rope project settings
    .ropeproject

    # mkdocs documentation
    /site

    # mypy
    .mypy_cache/
    .dmypy.json
    dmypy.json

    # Pyre type checker
    .pyre/
    `
}

function createGitlab():string{
    return expandToString`
    stages:
    - quality
    - deploy

    quality:
    stage: quality
    image: 
        name: sonarsource/sonar-scanner-cli:latest    
    variables:
        SONAR_USER_HOME: "\${CI_PROJECT_DIR}/.sonar" # Defines the location of the analysis task cache
        GIT_DEPTH: 0 # Tells git to fetch all the branches of the project, required by the analysis task
    cache:
        key: \${CI_JOB_NAME}
        paths:
        - .sonar/cache
    script:
        - sonar-scanner
    only:
        - master

    pypi:
    image: docker.km3net.de/base/python:3
    stage: deploy
    cache: {}
    before_script:
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - echo "$VAR_SSH_PRIVATE_KEY" | ssh-add -
        - git config --global user.email "\${GIT_USER_EMAIL}"
        - git config --global user.name "\${GIT_NAME}"        
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - touch ~/.ssh/known_hosts
        - echo 	"$KNOWN_HOSTS" >> ~/.ssh/known_hosts
    script:
        - pip install --upgrade pip
        - pip install -U twine
        - pip install --upgrade requests
        - rm -rf dist build
        - pip install commitizen
        - cz bump --yes        
        - python setup.py sdist        
        - twine upload dist/* -u "\${TWINE_USERNAME}" -p "\${TWINE_PASSWORD}" --skip-existing
    only:
        - master

    `
}

function createPoml():string {
    return expandToString`
    [tool.commitizen]
    name = "cz_conventional_commits"
    version = "0.0.1"
    tag_format = "v$version"
    bump_message = "release $current_version → $new_version"
    version_files = [
        "setup.py:version",
        "pyproject.toml:version",
        "sonar-project.properties:sonar.projectVersion",
    ]

    style = [
        ["qmark", "fg:#ff9d00 bold"],
        ["question", "bold"],
        ["answer", "fg:#ff9d00 bold"],
        ["pointer", "fg:#ff9d00 bold"],
        ["highlighted", "fg:#ff9d00 bold"],
        ["selected", "fg:#cc5454"],
        ["separator", "fg:#cc5454"],
        ["instruction", ""],
        ["text", ""],
        ["disabled", "fg:#858585 italic"]
    ]
    `
}

function createPublish (): string {
    return expandToString`
    rm -rf dist build
    python setup.py bdist_wheel
    python -m twine upload dist/*
    `
}

function createSetup(configuration: Configuration): string {
    return expandToString`
    from setuptools import setup, find_packages

    with open("README.md", "r") as fh:
        long_description = fh.read()

    setup(
        name='${configuration.software_name.toLocaleLowerCase()?? `#Software Name not configured`}',  # Required
        version='0.0.1',  # Required
        author="${configuration.author ?? `#Software Name not configured`}",
        author_email="${configuration.author_email ?? `#Author E-mail not configured`}"
        description="${configuration.about ?? `#About not configured`}",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="${configuration.repository ?? `#Repository configured`}",
        packages=find_packages(),
        
        install_requires=[
            'factory-boy'
        ],

        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
        setup_requires=['wheel'],
        
    )
    `
}

function createSonarProject(configuration: Configuration): string {
    return expandToString`
    sonar.projectKey=xxxx
    sonar.organization=xxxx
    sonar.projectVersion = 0.0.1
    sonar.sources = ${configuration.software_name.toLowerCase() ?? `#Software Name not configured`}
    sonar.language = py
    sonar.python.version = 3
    sonar.sourceEncoding = UTF-8
    sonar.host.url = http://sonarcloud.io
    `
}



