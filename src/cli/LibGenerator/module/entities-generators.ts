import fs from "fs";
import { createPath } from '../../generator-utils'
import { Application, isLocalEntity, isModule, LocalEntity } from "../../../language-server/generated/ast";
import path from 'path'
import { expandToString } from "langium";

export function generateEntities(application: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    
    if (application.configuration){
        
        const application_name = application.configuration.software_name

        const LIB_PATH = createPath(target_folder, "lib")
        const SOFTWARE_NAME_PATH = createPath(LIB_PATH,application_name)

        fs.writeFileSync(path.join(SOFTWARE_NAME_PATH, "__init__.py"), "");

        let entities: Array<LocalEntity> = []

        for (const module of application.abstractElements.filter(isModule))
        {
            for (const entity of module.elements.filter(isLocalEntity)){
                entities.push (entity);
                fs.writeFileSync(path.join(SOFTWARE_NAME_PATH,`${entity.name.toLowerCase()}.py`), generateModel(entity))
            }
        }

        fs.writeFileSync(path.join(SOFTWARE_NAME_PATH,`factories.py`), generateFactory(entities))


    }

    function generateFactoryClass(entity: LocalEntity):string {
        return expandToString`
        class ${entity.name}Factory(factory.Factory):
            class Meta:
                model = ${entity.name}
        `
    }

    function generateFactory(entities: LocalEntity[]):string{
        return expandToString`
        import factory
        ${entities.map(entity => `import .${entity.name.toLowerCase()} import ${entity.name}`).join("\n")}

        ${entities.map(entity => generateFactoryClass(entity)).join("\n")}
        
        `
    }

    function generateModel(entity: LocalEntity): string {
        return expandToString`
        import logging
        logging.basicConfig(level=logging.INFO)
        class ${entity.name}():
            def __init__(self):
                pass
            
            def get_all(self,today = False):
                try:
                    results = []
			        logging.info("Start function: get_all: ${entity.name}")
                    return results
                except Exception as e: 
                    logging.error("OS error: {0}".format(e))
                    logging.error(e.__dict__)
        `
    }
  
}