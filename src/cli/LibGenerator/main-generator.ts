import { Application } from "../../language-server/generated/ast";
import {generateEntities} from "./module/entities-generators"
import {generateHelpers} from "./module/helpers-generators"
import { generateDocumentation } from "./module/documentation-generator"

export function generateLib(application: Application, target_folder: string) : void {
    
    generateDocumentation(application,target_folder)
    generateEntities(application, target_folder)
    generateHelpers(application,target_folder)
     
}